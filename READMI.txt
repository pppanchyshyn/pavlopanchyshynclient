To run tests follow this instruction:
1 Run PavloPanchyshynService with command "mvn tomcat:run-war"
2 Run PavloPanchyshynClient:
    - To run all tests use command "mvn clean test"
    - To run soap service tests run command "mvn clean test -Dsuite=SoapSuite.xml"
    - To run rest service tests run command "mvn clean test -Dsuite=RestSuite.xml"
