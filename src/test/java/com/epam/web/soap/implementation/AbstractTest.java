package com.epam.web.soap.implementation;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public abstract class AbstractTest {
  final Logger logger = LogManager.getLogger(Const.class);
  UserService userService;

  @BeforeClass
  public void tearUp() {
    logger.info("Setting up remote web client");
    userService = new UserServiceImplService().getUserServiceImplPort();
    logger.info("Connection to remote web client successful\n");
    userService.signIn(Const.ADMIN_USER_NAME, Const.ADMIN_PASSWORD);
    userService.signIn(Const.USER2_NAME, Const.USER2_PASSWORD);
  }

  @AfterClass
  public void tearDown() {
    userService.signOut(Const.ADMIN_USER_NAME);
    userService.signOut(Const.USER2_NAME);
  }
}
