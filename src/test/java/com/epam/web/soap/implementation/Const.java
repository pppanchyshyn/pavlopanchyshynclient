package com.epam.web.soap.implementation;

public class Const {

  public static final String ADMIN_USER_NAME = "User1";
  public static final String ADMIN_PASSWORD = "U1111";
  public static final String USER2_NAME = "User2";
  public static final String USER3_NAME = "User3";
  public static final String USER2_PASSWORD = "U2222";
  public static final String FAKE_USER_NAME = "FakeUser";
  public static final String FAKE_PASSWORD = "Fake Password";
  public static final String SALES_ROLE = "Sales";
  public static final String FAKE_ROLE = "Fake";
  public static final int USERS_WITH_ROLE_SALES_NUMBER = 3;
  public static final String TEST_PASSED = "TEST PASSED \n " +
      "---------------------------------------------------";
  public static final String USER1_ROLE = "Admin";

  private Const() {
  }
}
