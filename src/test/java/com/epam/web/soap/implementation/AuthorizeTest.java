package com.epam.web.soap.implementation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AuthorizeTest extends AbstractTest {

  @Test
  public void successfulAuthorizationTest() {
    logger.info("Successful authorization test started...");
    logger.info("Trying to authorize admin in service...");
    Assert.assertNotNull(userService.authorize(Const.ADMIN_USER_NAME),
        "Fail. Unsuccessful authorization\n");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void correctAuthorizationTest() {
    logger.info("Correct authorization test started...");
    logger.info("Comparing actual User1 role and server response...");
    String response = userService.authorize(Const.ADMIN_USER_NAME).get(0);
    Assert
        .assertEquals(response, Const.USER1_ROLE, "Actual role and role from response different\n");
    logger.info(Const.TEST_PASSED);
  }
}
