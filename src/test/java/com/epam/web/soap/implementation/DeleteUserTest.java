package com.epam.web.soap.implementation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteUserTest extends AbstractTest {

  @Test
  public void successfulDeleteUserTest() {
    logger.info("SuccessfulDeleteUser test started");
    logger.info("Creating test user...");
    userService.createAccount(Const.FAKE_USER_NAME, Const.FAKE_PASSWORD, Const.SALES_ROLE);
    logger.info("Getting actual number of users");
    int actualUsersNumber = userService.getAllUsers(Const.ADMIN_USER_NAME).size();
    logger.info("Deleting test user");
    userService.deleteUser(Const.ADMIN_USER_NAME, Const.FAKE_USER_NAME);
    logger.info("Getting new number of users");
    int newUsersNumber = userService.getAllUsers(Const.ADMIN_USER_NAME).size();
    logger.info("Comparing users number before and after deleting...");
    Assert.assertTrue(actualUsersNumber > newUsersNumber,
        "Fail. Number of users in base before and after stay same");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void deletingAdminByItselfTest() {
    logger.info("deletingAdminByItself test started...");
    boolean isDeleted = userService.deleteUser(Const.ADMIN_USER_NAME, Const.ADMIN_USER_NAME);
    Assert.assertFalse(isDeleted, "Fail. Admin delete itself");
    logger.info("Admin was not deleted by itself");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void deletingUserWithOutAdminRightsTest() {
    logger.info("deletingUserWithOutAdminRights test started...");
    logger.info("Getting number of users before deleting");
    int usersNumberBeforeDeleting = userService.getAllUsers(Const.ADMIN_USER_NAME).size();
    boolean isDeleted = userService.deleteUser(Const.USER2_NAME, Const.USER3_NAME);
    logger.info("Getting number of users after deleting");
    int usersNumberAfterDeleting = userService.getAllUsers(Const.ADMIN_USER_NAME).size();
    Assert.assertFalse(isDeleted, "Fail. User can delete without admin rights");
    Assert.assertEquals(usersNumberBeforeDeleting, usersNumberAfterDeleting,
        "Fail. User without admin rights delete another user");
    logger.info("User without admin rights can't delete other user");
    logger.info(Const.TEST_PASSED);
  }
}
