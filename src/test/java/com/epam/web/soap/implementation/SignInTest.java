package com.epam.web.soap.implementation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SignInTest extends AbstractTest {

  @Test
  public void successfulSigningInTest() {
    logger.info("Successful signing in test started");
    logger.info("User try to sig in service with right credentials...");
    boolean isSignIn = userService.signIn(Const.USER2_NAME, Const.USER2_PASSWORD);
    Assert.assertTrue(isSignIn, "Fail. User with right credentials does not sign in");
    userService.signOut(Const.USER2_NAME);
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void unsuccessfulSigningInTest() {
    logger.info("Unsuccessful signing in test started...");
    logger.info("User try to sig in service with wrong password ...");
    boolean isSignIn = userService.signIn(Const.USER2_NAME, Const.FAKE_PASSWORD);
    Assert.assertFalse(isSignIn, "Fail. User with wrong credentials sign in");
    logger.info(Const.TEST_PASSED);
  }
}
