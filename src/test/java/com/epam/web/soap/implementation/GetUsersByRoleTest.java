package com.epam.web.soap.implementation;

import com.epam.web.soap.interfaces.User;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetUsersByRoleTest extends AbstractTest {

  @Test
  public void successfulGetUserByRoleTest() {
    logger.info("Successful getUserByRole test started...");
    logger.info("Trying to get data by admin...");
    List<User> response = userService.getUsersByRole(Const.ADMIN_USER_NAME, Const.SALES_ROLE);
    logger.info("Comparing real number of users with specified role and server response...");
    Assert.assertEquals(response.size(), Const.USERS_WITH_ROLE_SALES_NUMBER,
        "Fail. Incorrect number of users with role sales");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void getUserByFakeRoleTest() {
    logger.info("getUserByFakeRole test started...");
    logger.info("Trying to get data by admin...");
    List<User> response = userService.getUsersByRole(Const.ADMIN_USER_NAME, Const.FAKE_ROLE);
    Assert
        .assertTrue(response.isEmpty(), "Fail. List of users with not existent role is not empty");
    logger.info("Admin receives empty list of users with not existent role");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void getUserByRoleNotByAdminTest() {
    logger.info("getUserByRoleNotByAdmin test started...");
    logger.info("Trying to get data by user...");
    List<User> response = userService.getUsersByRole(Const.USER2_NAME, Const.SALES_ROLE);
    Assert
        .assertTrue(response.isEmpty(), "Fail. User who is not admin accessed secured data");
    logger.info("User receives empty list");
    logger.info(Const.TEST_PASSED);
  }
}
