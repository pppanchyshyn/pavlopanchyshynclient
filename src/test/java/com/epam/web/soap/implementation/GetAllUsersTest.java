package com.epam.web.soap.implementation;

import com.epam.web.soap.interfaces.User;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetAllUsersTest extends AbstractTest {

  @Test
  public void successfulGetAllUsersTest() {
    logger.info("Successful getAllUsers test started...");
    logger.info("Trying to get data by admin...");
    List<User> userList = userService.getAllUsers(Const.ADMIN_USER_NAME);
    Assert.assertNotNull(userList, "Fail. No one user was returned\n");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void getAllUserNotByAdminTest() {
    logger.info("getAllUsers by user, who is not admin testing...");
    logger.info("Trying to get data not by admin...");
    List<User> userList = userService.getAllUsers(Const.USER2_NAME);
    Assert.assertTrue(userList.isEmpty(), "Fail. User who is not admin accessed secured data\n");
    logger.info("User without admin rights receives empty list of users");
    logger.info(Const.TEST_PASSED);
  }
}
