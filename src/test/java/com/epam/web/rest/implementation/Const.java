package com.epam.web.rest.implementation;

import com.epam.web.rest.User;
import java.util.ArrayList;
import java.util.List;

public class Const {

  private static List<String> roles = new ArrayList<>();

  static {
    roles.add("Slave");
  }

  public static final User USER1_ADMIN = new User("User1", "U1111");
  public static final User USER2 = new User("User2", "U2222");
  public static final User USER5 = new User("User5", "U5555");
  public static final User FAKE_USER = new User("Fake", "Fake");
  public static final User NEW_USER = new User("User9", "U9999", roles);
  public static final String USER1_ROLE = "Admin";
  public static final String SALES_ROLE = "Sales";
  public static final String FAKE_ROLE = "Fake";
  public static final int USERS_NUMBER_WITH_SALES_ROLE = 3;
  public static final int USER1_POSITION_IN_LIST = 0;
  public static final int USER5_POSITION_IN_LIST = 4;
  public static final String TEST_PASSED = "TEST PASSED \n " +
      "---------------------------------------------------";

  private Const() {
  }
}
