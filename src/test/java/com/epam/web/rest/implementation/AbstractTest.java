package com.epam.web.rest.implementation;

import com.epam.web.rest.RestServiceClient;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public abstract class AbstractTest {

  final Logger logger = LogManager.getLogger(Const.class);
  RestServiceClient client;
  String adminToken;

  @BeforeClass
  public void tearUp() {
    client = new RestServiceClient();
    adminToken = client.signInAndGetToken(Const.USER1_ADMIN);
  }

  @AfterClass
  public void tearDown() {
    client.signOutResponse(adminToken);
  }
}
