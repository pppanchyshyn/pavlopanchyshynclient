package com.epam.web.rest.implementation;

import com.epam.web.rest.User;
import java.util.List;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetAllUsersTest extends AbstractTest {

  @Test
  public void successfulGetAllUsersTest() {
    logger.info("Successful getAllUsers test started...");
    logger.info("Trying to get data by admin...");
    Response response = client.getAllUsersResponse(adminToken);
    Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus(),
        "Fail. No one user was returned\n");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void correctGetAllUsersTest() {
    logger.info("Correct getAllUsers test started...");
    List<User> users = client.getAllUsersValue(adminToken);
    logger.info("Comparing actual User1 and server response...");
    Assert.assertEquals(Const.USER1_ADMIN, users.get(Const.USER1_POSITION_IN_LIST),
        "Actual User1 and server response are different\n");
    logger.info("Comparing actual User5 and server response...");
    Assert.assertEquals(Const.USER5, users.get(Const.USER5_POSITION_IN_LIST),
        "Actual User5 and server response are different\n");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void getAllUserNotByAdminTest() {
    logger.info("getAllUsers by user, who is not admin testing...");
    logger.info("Trying to get data not by admin...");
    String userToken = client.signInAndGetToken(Const.USER5);
    Response response = client.getAllUsersResponse(userToken);
    Assert.assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus(),
        "Fail. User who is not admin accessed secured data\n");
    logger.info("User without admin rights receives empty list of users");
    client.signOutResponse(userToken);
    logger.info(Const.TEST_PASSED);
  }
}
