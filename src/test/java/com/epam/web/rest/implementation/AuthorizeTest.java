package com.epam.web.rest.implementation;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AuthorizeTest extends AbstractTest {

  @Test
  public void successfulAuthorizationTest() {
    logger.info("Successful authorization test started...");
    logger.info("Trying to authorize admin in service...");
    Response response = client.authorizeResponse(adminToken);
    Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus(),
        "Fail. Unsuccessful authorization\n");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void correctAuthorizationTest() {
    logger.info("Correct authorization test started...");
    logger.info("Comparing actual User1 role and server response...");
    String role = client.authorizeValue(adminToken).get(0);
    Assert.assertEquals(role, Const.USER1_ROLE, "Actual role and role from response different\n");
    logger.info(Const.TEST_PASSED);
  }
}
