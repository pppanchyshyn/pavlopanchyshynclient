package com.epam.web.rest.implementation;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.testng.Assert;

import org.testng.annotations.Test;

public class DeleteUserTest extends AbstractTest {

  @Test
  public void successfulDeleteUserTest() {
    logger.info("SuccessfulDeleteUser test started");
    logger.info("Creating test user...");
    client.createAccountResponse(Const.NEW_USER);
    logger.info("Deleting test user");
    Response response = client.deleteUserResponse(adminToken, Const.NEW_USER.getName());
    Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus(),
        "Fail. User was not deleted");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void deletingAdminByItselfTest() {
    logger.info("deletingAdminByItself test started...");
    Response response = client.deleteUserResponse(adminToken, Const.USER1_ADMIN.getName());
    Assert.assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus(),
        "Fail. Admin delete itself");
    logger.info("Admin was not deleted by itself");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void deletingUserWithOutAdminRightsTest() {
    logger.info("deletingUserWithOutAdminRights test started...");
    String userToken = client.signInAndGetToken(Const.USER5);
    Response response = client.deleteUserResponse(userToken, Const.USER2.getName());
    Assert.assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus(),
        "Fail. User without admin rights delete another user");
    logger.info("User without admin rights can't delete other user");
    client.signOutResponse(userToken);
    logger.info(Const.TEST_PASSED);
  }
}
