package com.epam.web.rest.implementation;

import com.epam.web.rest.User;
import java.util.List;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetUsersByRoleTest extends AbstractTest {

  @Test
  public void successfulGetUsersByRoleTest() {
    logger.info("Successful getUserByRole test started...");
    logger.info("Trying to get data by admin...");
    Response response = client.getUsersByRoleResponse(adminToken, Const.SALES_ROLE);
    Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus(),
        "Fail admin do not access the data ");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void correctGetAllUsersTest() {
    logger.info("Correct getUserByRole test started...");
    logger.info("Trying to get data by admin...");
    List<User> users = client.getUsersByRoleValue(adminToken, Const.SALES_ROLE);
    logger.info("Comparing real number of users with specified role and server response...");
    Assert.assertEquals(Const.USERS_NUMBER_WITH_SALES_ROLE, users.size(),
        "Fail. Incorrect number of users with role sales");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void getUserByRoleNotByAdminTest() {
    logger.info("getUserByRoleNotByAdmin test started...");
    logger.info("Trying to get data by user...");
    String userToken = client.signInAndGetToken(Const.USER5);
    Response response = client.getUsersByRoleResponse(userToken, Const.SALES_ROLE);
    Assert.assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus(),
        "Fail. User who is not admin accessed secured data");
    logger.info("User receives empty list");
    client.signOutResponse(userToken);
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void getUserByFakeRoleTest() {
    logger.info("getUserByFakeRole test started...");
    logger.info("Trying to get data by admin...");
    Response response = client.getUsersByRoleResponse(adminToken, Const.FAKE_ROLE);
    Assert.assertEquals(Status.NO_CONTENT.getStatusCode(), response.getStatus(),
        "Fail. List of users with not existent role is not empty");
    logger.info("Admin receives empty list of users with not existent role");
    logger.info(Const.TEST_PASSED);
  }
}
