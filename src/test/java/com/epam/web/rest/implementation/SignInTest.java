package com.epam.web.rest.implementation;

import java.util.Objects;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SignInTest extends AbstractTest {

  @Test
  public void successfulSigningInTest() {
    logger.info("Successful signing in test started");
    logger.info("User try to sig in service with right credentials...");
    Assert.assertTrue(Objects.nonNull(adminToken),
        "Fail. User with right credentials does not sign in");
    logger.info(Const.TEST_PASSED);
  }

  @Test
  public void unsuccessfulSignInTest() {
    logger.info("Unsuccessful signing in test started...");
    logger.info("User try to sig in service with wrong credentials...");
    Response response = client.signIn(Const.FAKE_USER);
    Assert.assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.getStatus(),
        "Fail. User with wrong credentials sign in");
    logger.info(Const.TEST_PASSED);
  }
}
