package com.epam.web.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.CollectionType;

public class RestServiceClient {

  private static final String URL = "http://localhost:8080/PavloPanchyshynService/UsersBaseREST";
  private static final String CREATE_ACCOUNT = "/user";
  private static final String SIGN_IN = "/authentication";
  private static final String AUTHORIZE = "/user/roles";
  private static final String GET_ALL_USERS = "/users";
  private static final String GET_USERS_BY_ROLE = "/users/roles";
  private static final String DELETE_USER = "/users";
  private static final String SIGN_OUT = "/signOut";
  private List<Object> providers;

  public RestServiceClient() {
    providers = new ArrayList<>();
    providers.add(new JacksonJsonProvider());
  }

  public Response createAccountResponse(User user) {
    WebClient client = WebClient.create(URL + CREATE_ACCOUNT, providers);
    return client.accept("application/json; charset=UTF-8").type("application/json").post(user);
  }

  public Response signIn(User user) {
    WebClient client = WebClient.create(URL + SIGN_IN, providers);
    return client.accept("text/plain; charset=UTF-8").type("application/json").post(user);
  }

  public String signInAndGetToken(User user) {
    String value = null;
    try {
      value = IOUtils.toString((InputStream) signIn(user).getEntity());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return value;
  }

  public Response authorizeResponse(String token) {
    WebClient client = WebClient.create(URL + AUTHORIZE, providers);
    return client.accept("application/json; charset=UTF-8").header("token", token).get();
  }

  public List<String> authorizeValue(String token) {
    MappingJsonFactory factory = new MappingJsonFactory();
    Response response = authorizeResponse(token);
    List value = new ArrayList();
    try {
      JsonParser parser = factory.createJsonParser((InputStream) response.getEntity());
      value = parser.readValueAs(List.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return getStringParametrizedList(value);
  }

  public Response getAllUsersResponse(String token) {
    WebClient client = WebClient.create(URL + GET_ALL_USERS, providers);
    return client.accept("application/json; charset=UTF-8").header("token", token).get();
  }

  public List<User> getAllUsersValue(String token) {
    MappingJsonFactory factory = new MappingJsonFactory();
    Response response = getAllUsersResponse(token);
    List value = new ArrayList<String>();
    try {
      JsonParser parser = factory.createJsonParser((InputStream) response.getEntity());
      value = parser.readValueAs(List.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return getUserParametrizedList(value);
  }

  public Response getUsersByRoleResponse(String token, String role) {
    WebClient client = WebClient.create(URL + GET_USERS_BY_ROLE, providers);
    return client.accept("application/json; charset=UTF-8").header("token", token)
        .query("role", role).get();
  }

  public List<User> getUsersByRoleValue(String token, String role) {
    MappingJsonFactory factory = new MappingJsonFactory();
    Response response = getUsersByRoleResponse(token, role);
    List value = new ArrayList<String>();
    try {
      JsonParser parser = factory.createJsonParser((InputStream) response.getEntity());
      value = parser.readValueAs(List.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return getUserParametrizedList(value);
  }

  public Response deleteUserResponse(String token, String deletedUserName) {
    WebClient client = WebClient.create(URL + DELETE_USER, providers);
    return client.accept("application/json; charset=UTF-8").header("token", token)
        .query("name", deletedUserName).delete();
  }

  public Response signOutResponse(String token) {
    WebClient client = WebClient.create(URL + SIGN_OUT, providers);
    return client.accept("application/json; charset=UTF-8").header("token", token).get();
  }

  private List<User> getUserParametrizedList(List list) {
    List<User> parametrizedList = new ArrayList<>();
    ObjectMapper mapper = new ObjectMapper();
    try {
      String jsonArray = mapper.writeValueAsString(list);
      CollectionType javaType = mapper.getTypeFactory()
          .constructCollectionType(List.class, User.class);
      parametrizedList = mapper.readValue(jsonArray, javaType);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return parametrizedList;
  }

  private List<String> getStringParametrizedList(List list) {
    List<String> parametrizedList = new ArrayList<>();
    ObjectMapper mapper = new ObjectMapper();
    try {
      String jsonArray = mapper.writeValueAsString(list);
      CollectionType javaType = mapper.getTypeFactory()
          .constructCollectionType(List.class, String.class);
      parametrizedList = mapper.readValue(jsonArray, javaType);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return parametrizedList;
  }
}
