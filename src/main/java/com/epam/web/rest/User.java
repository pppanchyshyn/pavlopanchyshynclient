package com.epam.web.rest;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class User implements Serializable{

  private String name;
  private String password;
  private List<String> roles;
  private String token;

  public User() {
  }

  public User(String name, String password, List<String> roles) {
    this.name = name;
    this.password = password;
    this.roles = roles;
  }

  public User(String name, String password) {
    this.name = name;
    this.password = password;
  }

  public User(String name, String password, String roles) {
    this.name = name;
    this.password = password;
    this.roles = Arrays.asList(roles.split("\\s*,\\s*"));
  }

  public User(String name, String password, String roles, String token) {
    this.name = name;
    this.password = password;
    this.roles = Arrays.asList(roles.split("\\s*,\\s*"));
    this.token = token;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(name, user.name) &&
        Objects.equals(password, user.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, password);
  }

  @Override
  public String toString() {
    return String.format("User Name: %-10sPassword: %-10sRoles: %-40s", name, password, roles);
  }
}
